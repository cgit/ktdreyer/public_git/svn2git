%global gem_name svn2git

Name: %{gem_name}
Version: 2.3.2
Release: 3%{?dist}
Summary: A tool for migrating SVN projects to Git
Group: Development/Languages
License: MIT
URL: https://github.com/nirvdrum/svn2git
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: git-svn
%if 0%{?el7}
Requires: ruby(release)
Requires: ruby(rubygems)
Requires: rubygem(open4)
%endif
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: rubygem(minitest)
BuildRequires: rubygem(open4)
BuildArch: noarch
%if 0%{?el7}
Provides: rubygem(%{gem_name}) = %{version}
%endif

# The ruby(release) package already provides a usable Ruby interpreter.
# Filter the extra /usr/bin/ruby requirement here.
%global __requires_exclude ^/usr/bin/ruby$

%description
A tool for migrating SVN projects to Git.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

# Remove /usr/bin/env from shebang so RPM doesn't consider this a dependency
sed -i 's|#!/usr/bin/env ruby|#!/usr/bin/ruby|' bin/svn2git

# Remove unnecessary developer-only files
rm Rakefile
rm VERSION.yml
sed -i 's|"VERSION.yml",||' %{gem_name}.gemspec
sed -i 's|"Rakefile",||' %{gem_name}.gemspec

%build
gem build %{gem_name}.gemspec

%gem_install

# Remove unecessary gemspec
pushd .%{gem_instdir}
  rm %{gem_name}.gemspec
popd

%install
mkdir -p %{buildroot}%{gem_dir}
cp -pa .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

mkdir -p %{buildroot}%{_bindir}
cp -pa .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{gem_instdir}/bin -type f | xargs chmod a+x

%check
pushd .%{gem_instdir}
  ruby -e 'Dir.glob "./test/*_test.rb", &method(:require)'
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/MIT-LICENSE
%doc %{gem_instdir}/README.markdown
%{_bindir}/svn2git
%{gem_instdir}/bin
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/ChangeLog.markdown
%exclude %{gem_instdir}/test

%changelog
* Mon Jan 11 2016 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.3.2-3
- Drop Fedora 20 conditionals
- Drop unneeded %%license definition

* Tue Mar 24 2015 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.3.2-2
- Drop Fedora 19 conditionals
- Use %%license macro

* Fri Jan 02 2015 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.3.2-1
- Update to latest upstream version
- Rename to svn2git (RHBZ #1015253)
- Add requirement on git-svn
- Add Requires: for popen4 for dists older than Fedora 21

* Wed May 21 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.3.1-1
- Update to latest upstream version
- Drop Minitest 5 patch (merged upstream)
- Adjustments for https://fedoraproject.org/wiki/Changes/Ruby_2.1

* Tue Apr 08 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.2.5-1
- Update to latest upstream version
- Patch for Minitest 5 support

* Sat Nov 23 2013 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.2.2-2
- Remove Rakefile during %%prep
- Move README to main binary package
- Remove gem2rpm comment

* Tue Sep 24 2013 Ken Dreyer <ktdreyer@ktdreyer.com> - 2.2.2-1
- Initial package
